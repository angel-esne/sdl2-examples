
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\
 *                                                                           *
 *  Este código es de dominio público.                                       *
 *  angel.rodriguez@esne.edu                                                 *
 *  2015.04                                                                  *
 *                                                                           *
 *  Antes de poder ejecutar el programa, las bibliotecas de enlace dinámico  *
 *  que se encuentran en la subcarpeta \libraries\windows\sdl2-mixer-vs\bin  *
 *  deben colocarse junto al ejecutable compilado o en alguna  de las rutas  *
 *  en las que el sistema operativo espera encontrarlas (\Windows\System32). *
 *                                                                           *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <SDL.h>
#include <SDL_mixer.h>

int main (int , char ** )
{
    // Se inicializa SDL:

    if (SDL_Init (SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
    {
        SDL_Log ("No se ha podido inicializar SDL2.");
    }
    else
    {
        if (Mix_OpenAudio (44100, MIX_DEFAULT_FORMAT, 2, 4096) < 0)
        {
            SDL_Log ("No se ha podido inicializar el subsistema de audio.");
        }
        else
        {
            // Se crea una ventana:

            SDL_Window * window = SDL_CreateWindow
            (
                "SDL2 Audio Playback Example", 
                SDL_WINDOWPOS_UNDEFINED, 
                SDL_WINDOWPOS_UNDEFINED, 
                800, 
                600, 
                SDL_WINDOW_SHOWN
            );

            if (not window)
            {
                SDL_Log ("No se ha podido crear una ventana.");
            }
            else
            {
                // Se toma el buffer de la ventana para dibujar en él:

                SDL_Surface * surface = SDL_GetWindowSurface (window);

                if (not surface)
                {
                    SDL_Log ("No se ha podido acceder al buffer de la ventana.");
                }
                else
                {
                    // Se intenta cargar un sonido y una música:

                    Mix_Music * music = nullptr;
                    Mix_Chunk * sound = nullptr;

                    if
                    (
                        not (music = Mix_LoadMUS ("../../assets/rainforest-ambience.ogg")) ||
                        not (sound = Mix_LoadWAV ("../../assets/throw-knife.wav"        ))
                    )
                    {
                        SDL_Log ("No se ha podido cargar el audio.");
                    }
                    else
                    {
                        // Se inicia la reproducción de la música en bucle con un fundido:

                        Mix_FadeInMusic (music, -1, 4000);

                        bool exit = false;

                        do
                        {
                            // Se procesan los eventos acumulados:

                            SDL_Event event;

                            while (SDL_PollEvent (&event) > 0)
                            {
                                switch (event.type)
                                {
                                    case SDL_MOUSEBUTTONDOWN:
                                    {                                       // Cuando se hace click con algún botón del ratón
                                        Mix_PlayChannel (-1, sound, 0);     // se reproduce un sonido en un canal libre
                                        break;
                                    }

                                    case SDL_QUIT:
                                    {
                                        exit = true;
                                        break;
                                    }
                                }
                            }

                            // Se rellena el fondo de la ventana de un color color que depende de
                            // si se está reproduciendo algún sonido o no:

                            SDL_FillRect (surface, 0, SDL_MapRGB (surface->format, 225, Mix_Playing (-1) ? 200 : 225, 200));

                            SDL_UpdateWindowSurface (window);
                        }
                        while (!exit);

                        Mix_HaltMusic   ();
                        Mix_HaltChannel (-1);
                    }

                    if (music) Mix_FreeMusic (music);
                    if (sound) Mix_FreeChunk (sound);
                }

                SDL_DestroyWindow (window);
            }

            Mix_CloseAudio ();
        }

        SDL_Quit ();
    }

    return 0;
}
